package hello;

import java.net.*;

import javax.servlet.http.*;

import redis.clients.jedis.*;

import org.slf4j.*;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;

/**
 * Controller.
 */
@RestController
public class HelloController {

    private Logger logger = LoggerFactory.getLogger(HelloController.class);

    /**
     * The client for Redis operation.
     */
    private Jedis redisClient = null;

    /**
     * Origin request.
     */
    @Autowired
    private HttpServletRequest request;

    /**
     * Returns a Redis client.
     * @return Redis client.
     */
    private synchronized Jedis getRedis()
    {
        if(redisClient == null)
        {
            try
            {
                redisClient = new Jedis("redis", 6379);
            }
            catch(Exception e)
            {
                logger.error("Failed to initialized redis client: " +
                        e.toString());
            }
        }
        return redisClient;
    } 

    /**
     * Handler for index page.
     */
    @RequestMapping("/")
    public String index() {
        String hostname = null;
        Jedis redis = getRedis();

        /*
        HttpServletRequest request =
            ((ServletRequestAttributes)RequestContextHolder.
                getRequestAttributes()).getRequest();
        */
        String ip = request.getHeader("X-FORWARDED-FOR");
        if(ip == null) {
            ip = request.getRemoteAddr();
        }
        else {
            ip = ip.split(", ")[0];
        }

        logger.info("Receive request from " + ip);
        
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e) {
            hostname = "(null)";
        }
        
        String key = "visited_" + hostname;

        StringBuilder builder = new StringBuilder(
                "<html><head><title>Hello</title></head>");

        builder.append("<body><p>Hello from container ");
        builder.append(hostname);
        builder.append("</p>");

        if(redisClient != null)
        {
            try
            {
                long value = redisClient.incr(key);
                builder.append("<p>Visited ");
                builder.append(value);
                builder.append(" times</p>");
            }
            catch(Exception e)
            {
                logger.error("Failed to increment value in redis client: " +
                        e.toString());
            }
        }

        builder.append("</body></html>");

        return builder.toString();
    }
}

