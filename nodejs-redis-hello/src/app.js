var express = require('express');
var redis = require('redis');
var os = require('os');

var redisClient = redis.createClient(6379, 'redis')
var app = express();
var hostname = os.hostname();
var key = 'visit_' + hostname

process.on('SIGINT', function() {
  console.log('Caught SIGINT, exiting');

  process.exit();
});

process.on('SIGTERM', function() {
  console.log('Caught SIGTERM, exiting');

  process.exit();
});

app.get('/', function(req, res)
{
  redisClient.get(key, function(err, reply) 
  {
    nb = reply != null ? reply : 0;

    nb++;

    res.send('<html>' + 
      '<head><title>Hello</title></head>' +
      '<body>' +
      '<p>Hello from container ' + hostname + '</p>' +
      (err == null ? '<p>Visited ' + nb + ' times</p>' : '') +
      '</body>' +
      '</html>');

    redisClient.incr(key, function(err, reply)
      {
        console.log('Increment key ' + key);
      })
  })
});

app.listen(8080);
console.log('Hello nodejs app running');

