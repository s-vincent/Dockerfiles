#!/bin/sh

for DIR in */
do
  echo "Build ${DIR} image"
  cd ${DIR} && ./build.sh ; cd ..
done

