package main

import (
  "fmt"
  "os"
  "strings"
  "net"
  "net/http"
  "os/signal"
  "syscall"
  "time"
  "runtime/debug"
)

var g_run bool
var g_mem_run bool

func cpu() {
  cpt := 0
  for g_run {
    cpt++;
  }
}

func mem() {
  var (
    malloc_size = 1024 * 1000 * 250
    chunk []byte
  )
  chunk = make([]byte, malloc_size)

  for i := 0 ; i < malloc_size ; i++ {
    chunk[i] = byte('a')
  }

  for g_mem_run {
    time.Sleep(1 * time.Second)
  }

  fmt.Println("Free memory")
  chunk = nil
  debug.FreeOSMemory()
}

func handlerMem(resp http.ResponseWriter, req *http.Request) {
  fmt.Println("Hello golang mem up")
  g_mem_run = true
  go mem()
}

func handlerUnmem(resp http.ResponseWriter, req *http.Request) {
  fmt.Println("Hello golang mem down")
  g_mem_run = false
}

func handlerUp(resp http.ResponseWriter, req *http.Request) {
  fmt.Println("Hello golang cpu up")
  g_run = true
  go cpu()
}

func handlerDown(resp http.ResponseWriter, req *http.Request) {
  fmt.Println("Hello golang cpu down")
  g_run = false
}

func handler(resp http.ResponseWriter, req *http.Request) {
  hostname, _ := os.Hostname()

  ip := strings.Split(req.Header.Get("X-FORWARDED-FOR"), ", ")[0]

  if ip == "" {
    ip, _, _ = net.SplitHostPort(req.RemoteAddr)
  }

  fmt.Println("Receive request from", ip)

  fmt.Fprintf(resp, "<html>" +
    "<head><title>Hello</title></head>" +
    "<body><p>Hello from container " + hostname + "</p></body>" +
    "</html>")
}

func main() {
  fmt.Println("Hello golang app running")

  // catch signals
  sigs := make(chan os.Signal, 1)
  signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

  go func() {
    s := <-sigs
    fmt.Println("Receive signal", s)
    os.Exit(1)
  }()

  g_run = true
  go cpu()

  http.HandleFunc("/", handler)
  http.HandleFunc("/up", handlerUp)
  http.HandleFunc("/down", handlerDown)
  http.HandleFunc("/mem", handlerMem)
  http.HandleFunc("/unmem", handlerUnmem)
  http.ListenAndServe(":8080", nil)
}

