package hello;

import java.net.*;

import javax.servlet.http.*;

import org.slf4j.*;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;

@RestController
public class HelloController {

    private Logger logger = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    private HttpServletRequest request;

    @RequestMapping("/")
    public String index() {
        String hostname = null;
        /*
        HttpServletRequest request =
            ((ServletRequestAttributes)RequestContextHolder.
                getRequestAttributes()).getRequest();
        */
        String ip = request.getHeader("X-FORWARDED-FOR");
        if(ip == null) {
            ip = request.getRemoteAddr();
        }
        else {
            ip = ip.split(", ")[0];
        }

        logger.info("Receive request from " + ip);
        
        try {
            hostname = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e) {
            hostname = "(null)";
        }

        return "<html><head><title>Hello</title></head>" +
            "<body><p>Hello from container " + hostname + "</p></body></html>";
    }
}

