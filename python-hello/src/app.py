from flask import Flask, request
import os
import signal

app = Flask('python-hello')

def signal_handler(signum, frame):
    if signum == signal.SIGTERM or signum == signal.SIGINT:
        raise SystemExit

@app.route('/')
def hello():
    hostname = os.uname()[1]

    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr

    print('Receive request from {}'.format(ip), flush = True)

    content = '<html><head><title>Hello</title></head>';
    content += '<body><p>Hello from container '
    content += hostname
    content += '</p></body></html>'
 
    return content

if __name__ == '__main__':
    print("Hello python app running");

    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)

    app.run(host='0.0.0.0', port=8080)

