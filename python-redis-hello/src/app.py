from flask import Flask, request
import os
import signal
import redis

app = Flask('python-hello')
hostname = os.uname()[1]
redis_client = redis.StrictRedis(host='redis', port=6379, db=0)
key = 'visit_' + hostname

def signal_handler(signum, frame):
    if signum == signal.SIGTERM or signum == signal.SIGINT:
        raise SystemExit

@app.route('/')
def hello():

    if request.headers.getlist("X-Forwarded-For"):
        ip = request.headers.getlist("X-Forwarded-For")[0]
    else:
        ip = request.remote_addr

    print('Receive request from {}'.format(ip), flush=True)

    value = redis_client.incr(key);

    content = '<html><head><title>Hello</title></head>';
    content += '<body><p>Hello from container ' + hostname + '</p>'

    if value == None:
        value = 0

    content += '<p>Visited ' + str(value) + ' times</p>'
    content += '</body></html>'

    return content

if __name__ == '__main__':
    print("Hello python app running");

    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)

    app.run(host='0.0.0.0', port=8080)

