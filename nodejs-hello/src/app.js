var express = require('express');
var os = require('os');

var app = express();
var hostname = os.hostname();

process.on('SIGINT', function() {
  console.log('Caught SIGINT, exiting');

  process.exit();
});

process.on('SIGTERM', function() {
  console.log('Caught SIGTERM, exiting');

  process.exit();
});

app.get('/', function(req, res)
{
  var ips = req.headers['x-forwarded-for'];
  var ip = "";

  if(ips) {
    ips = ips.split(", ")
    ip = ips[0];
  }
  else {
    ip = req.connection.remoteAddress;
  }

  console.log('Receive request from ' + ip);
	res.send('<html>' + 
    '<head><title>Hello</title></head>' +
    '<body><p>Hello from container ' + hostname + '</p></body>' +
    '</html>');
});

app.listen(8080);
console.log('Hello nodejs app running');

