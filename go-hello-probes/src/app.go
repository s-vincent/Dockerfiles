package main

import (
  "fmt"
  "os"
  "os/signal"
  "syscall"
  "strings"
  "net"
  "net/http"
)

func handlerGood(resp http.ResponseWriter, req *http.Request) {
  fmt.Fprintf(resp, "OK");
}

func handlerBad(resp http.ResponseWriter, req *http.Request) {
  http.Error(resp, "NOK", http.StatusInternalServerError)
}

func handler(resp http.ResponseWriter, req *http.Request) {
  hostname, _ := os.Hostname()

  ip := strings.Split(req.Header.Get("X-FORWARDED-FOR"), ", ")[0]

  if ip == "" {
    ip, _, _ = net.SplitHostPort(req.RemoteAddr)
  }

  fmt.Println("Receive request from", ip)

  fmt.Fprintf(resp, "<html>" +
    "<head><title>Hello</title></head>" +
    "<body><p>Hello from container " + hostname + "</p></body>" +
    "</html>")
}

func main() {
  fmt.Println("Hello golang app running");

  // catch signals
  sigs := make(chan os.Signal, 1)
  signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

  go func() {
    s := <-sigs
    fmt.Println("Receive signal", s)
    os.Exit(1)
  }()

  http.HandleFunc("/", handler)
  http.HandleFunc("/health", handlerGood)
  http.HandleFunc("/bad", handlerBad)
  http.ListenAndServe(":8080", nil)
}

