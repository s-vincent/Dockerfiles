package main

import (
  "fmt"
  "strconv"
  "os"
  "os/signal"
  "syscall"
  "strings"
  "net"
  "net/http"
  "context"
  "github.com/go-redis/redis/v8"
)


var ctx = context.Background()
var redisClient *redis.Client
var hostname, _ = os.Hostname()
var key = "visit_" + hostname

func handler(resp http.ResponseWriter, req *http.Request) {
  ip := strings.Split(req.Header.Get("X-FORWARDED-FOR"), ", ")[0]
  res := ""

  if ip == "" {
    ip, _, _ = net.SplitHostPort(req.RemoteAddr)
  }

  fmt.Println("Receive request from", ip)

  nb, err := redisClient.Get(ctx, key).Int()

  if err != nil {
    fmt.Println(err)
    nb = 1;
  }
  res = "<p>Visited " + strconv.Itoa(nb)  + " times</p>"

  fmt.Println("truc", nb);

  fmt.Fprintf(resp, "<html>" +
    "<head><title>Hello</title></head>" +
    "<body>" +
    "<p>Hello from container " + hostname + "</p>" +
    res +
    "</body>" +
    "</html>")

    nb = nb + 1
    err = redisClient.Set(ctx, key, nb, 0).Err()
}

func main() {
  fmt.Println("Hello golang app running");

  // catch signals
  sigs := make(chan os.Signal, 1)
  signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

  go func() {
    s := <-sigs
    fmt.Println("Receive signal", s)
    os.Exit(1)
  }()

  redisClient = redis.NewClient(&redis.Options{
    Addr: "redis:6379",
  })

  pong, err := redisClient.Ping(ctx).Result()
  fmt.Println(pong, err)

  http.HandleFunc("/", handler)
  http.ListenAndServe(":8080", nil)
}

