#!/bin/sh

ARGS="$#"
REPO=svincent

if [ ${ARGS} -ge 1 ]; then
  REPO=$1
fi

echo "Starts tag and push in ${REPO}/"

#IMGS=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep -v ${REPO} | grep -v hello-world | grep hello)
IMGS="go-hello:1.0 go-hello:2.0 \
  go-hello:env go-hello:probes \
  python-hello:1.0 python-hello:2.0 \
  nodejs-hello:1.0 nodejs-hello:2.0 \
  springboot-hello:1.0 springboot-hello:2.0 \
  cpu:1.0"

for IMG in ${IMGS}
do
  echo docker tag ${IMG} ${REPO}/${IMG}
  docker tag ${IMG} ${REPO}/${IMG}
  docker push ${REPO}/${IMG}
  docker image rm ${REPO}/${IMG}
done

echo "Ends"

