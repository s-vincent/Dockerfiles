#!/bin/sh

for d in */
do
  echo "Build $d image"
  cd $d && docker run --rm -i hadolint/hadolint < ./Dockerfile ; cd ..
done
