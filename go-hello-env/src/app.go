package main

import (
  "fmt"
  "os"
  "strings"
  "net"
  "net/http"
  "os/signal"
  "syscall"
)

func handler(resp http.ResponseWriter, req *http.Request) {
  hostname, _ := os.Hostname()
  env := ""

  ip := strings.Split(req.Header.Get("X-FORWARDED-FOR"), ", ")[0]

  if ip == "" {
    ip, _, _ = net.SplitHostPort(req.RemoteAddr)
  }

  fmt.Println("Receive request from", ip)

  for _, e := range os.Environ() {
    env += fmt.Sprintf("<p>%s</p>", e)
  }

  fmt.Fprintf(resp, "<html>" +
    "<head><title>Hello</title></head>" +
    "<body>" +
    "<p>Hello from container " + hostname + "</p>" +
    env +
    "</body>" +
    "</html>")
}

func main() {
  fmt.Println("Hello golang app running")

  // catch signals
  sigs := make(chan os.Signal, 1)
  signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

  go func() {
    s := <-sigs
    fmt.Println("Receive signal", s)
    os.Exit(1)
  }()

  http.HandleFunc("/", handler)
  http.ListenAndServe(":8080", nil)
}

